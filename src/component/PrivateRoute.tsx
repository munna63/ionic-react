import React, { Component } from 'react';
import { BrowserRouter as Router, Redirect, Route, Switch } from 'react-router-dom';
import { getToken } from './header';

export const PrivateRoute= ({component: Component, ...rest}) =>{
  return (
    <Route
      {...rest}
      render={(props) => 
        getToken() ? (<Component {...props} />) :(<Redirect to={{pathname: '/signin', state: {from: props.location}}} />)
      }
    />
  )
}