import Cookies from 'js-cookie';
import React from 'react';
import * as constant from '../constants/App_props';

import {
    IonPage,
    IonContent,
    IonHeader,
    IonTitle,
    IonToolbar,
    IonButton,
    IonCol,
    IonRow,
    IonInput,
    IonText,
} from '@ionic/react';

class Logout extends React.Component{
    state: any = {};
    props: any = {};
    constructor(props: any) {
        super(props);
        this.state = {}
    }

    logout = ()=>{
        Cookies.remove(constant.C_USERNAME);
        Cookies.remove(constant.C_ACCESS_TOKEN);
        Cookies.remove(constant.C_REFRESH_TOKEN);
        
        if(this.state.isSignedIn){
            console.log('invalidating google token')
            this.state.googleLogin.signOut();
        }
        window.location.href = '/signin';
    }

    render() {
       return(
        <IonContent>
            <IonRow class="row">
            <IonButton onClick={this.logout} type="button" expand="block" color="undefined" class="btn-transition"><strong className="white">Logout</strong></IonButton>
            </IonRow>
        </IonContent>
       ) 
    }
}

export default Logout;