import Cookies from 'js-cookie';
import { Component } from 'react';
import * as constant from '../constants/App_props';

class header extends Component {
    state: any = {};
    props: any = {};
    constructor(props: any) {
        super(props);
        this.state = {}
    }

}
export const getToken = () => {
    return Cookies.get(constant.C_ACCESS_TOKEN);
}
export let headers = {
    'Accept': 'application/json',
    'Content-Type': 'application/json',
    'Access-Control-Allow-Origin' : '*',
    "Authorization" : 'Bearer '+Cookies.get(constant.C_ACCESS_TOKEN)
}