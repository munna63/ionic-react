import React, { Component } from "react"
import axios from 'axios'
import * as ROUTES from '../../constants/routes';
import Cookies from 'js-cookie';
import * as constant from '../../constants/App_props';
import InfiniteScroll from "react-infinite-scroll-component";
import { IonAlert, IonButton, IonItem } from "@ionic/react";
import Logout from "../../component/logout";
import {GeolocationOptions, Plugins} from '@capacitor/core';
const {Geolocation, Modals} = Plugins;

const style = {
  height: 30,
  border: "1px solid green",
  margin: 6,
  padding: 8
};

class Maps extends Component <any,any>{
  state: any = {};
  lat :any = "";
  long :any = "";
  constructor(props:any) {
    super(props)
    this.state = "";
  }

  
    componentDidMount(){
      this.getCurrentPosition();
    }

    async getCurrentPosition(){
      let geoOptions : GeolocationOptions = {
        enableHighAccuracy:true
      } 
      const coordinates = await Geolocation.getCurrentPosition(geoOptions);
      //this.showAlert(coordinates);
      this.lat = coordinates.coords.latitude;
      this.long = coordinates.coords.longitude;
      this.setState({lat:this.lat,long : this.long});
      console.log(this.lat, this.long);
    }

    async showAlert(msg){
      let alert = await Modals.alert({
        title:'Alert',
        message:msg
      })
    }

    render(){
      return (
        <div>
          
        <h1>lat : {this.state.lat}</h1>
        <br/>
        <h1>long : {this.state.long}</h1>
            <IonButton routerLink="/maps" routerDirection="root" type="button" id='' expand="block" class="btn-transition">
              <strong className="white">Show My Location</strong>
            </IonButton>
              <Logout></Logout>
            
            
            
        </div>
      );
    }
}
export default Maps;