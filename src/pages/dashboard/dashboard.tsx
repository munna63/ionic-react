import React, { Component } from "react"
import axios from 'axios'
import * as ROUTES from '../../constants/routes';
import Cookies from 'js-cookie';
import * as constant from '../../constants/App_props';
import InfiniteScroll from "react-infinite-scroll-component";
import { IonAlert, IonButton } from "@ionic/react";
import Logout from "../../component/logout";
import {GeolocationOptions, Plugins} from '@capacitor/core';

const {Geolocation, Modals} = Plugins;

const style = {
  height: 30,
  border: "1px solid green",
  margin: 6,
  padding: 8
};

class Dashboard extends Component <any,any>{
  constructor(props:any) {
    super(props)
    this.state = { items: [] ,hasMore: true}
  }

  fetchMoreData = ()=>{
    console.log('current length of data ' +this.state.items.length)
    console.log('total length of data ' +this.state.totalHits)
    if(this.state.items.length == this.state.totalHits){
      console.log('has more false');
      this.setState({ hasMore: false });
      return;
    }
    this.setState({pageNo : this.state.pageNo+1});
    this.fetchSingleSlotOfData(this.state.pageNo);
  }

  fetchSingleSlotOfData = (pageNo)=>{
    axios.post(ROUTES.DASHBOARD_PROJECTLIST,{pageNo: pageNo,pageSize: constant.DATA_PAGE_SIZE})
      .then((res) =>{
        if(res.data && res.data.body){
          this.setState({items:this.state.items.concat(res.data.body)});
          this.setState({totalHits : res.data.totalHits});
        }
      }).catch((err) =>{
        if(err.message == 'Network Error'){
          alert('A network error occured');
        }
        console.log(err.message);
      });
  }
    componentDidMount(){
      this.fetchSingleSlotOfData(0);
      this.setState({pageNo : 0});
      this.getCurrentPosition();
    }

    async getCurrentPosition(){
      let geoOptions : GeolocationOptions = {
        enableHighAccuracy:true
      } 
      const coordinates = await Geolocation.getCurrentPosition(geoOptions);
      //this.showAlert(coordinates);
      
      console.log(coordinates.coords.longitude);
    }

    async showAlert(msg){
      let alert = await Modals.alert({
        title:'Alert',
        message:msg
      })
    }

    render(){
      return (
        <div>
          
            <InfiniteScroll
              dataLength={this.state.items.length}
              next={this.fetchMoreData}
              hasMore={this.state.hasMore}
              loader={<h4>Loading...</h4>}
              height={400}
              endMessage={
                <p style={{ textAlign: "center" }}>
                  <b>Yay! You have seen it all</b>
                </p>
              }>
              {
                this.state.items.map(project  => (
                  <div style={style} key={project.id}>
                    {project.id} - #{project.projectName}
                  </div>
                ))
              }
            </InfiniteScroll>
            <IonButton routerLink="/maps" routerDirection="root" type="button" id='' expand="block" class="btn-transition">
              <strong className="white">Show My Location</strong>
            </IonButton>
              <Logout></Logout>
            
            
            
        </div>
      );
    }
}
export default Dashboard;