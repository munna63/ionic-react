/* global gapi */
import React, { Component } from 'react';
import {
    IonPage,
    IonContent,
    IonHeader,
    IonTitle,
    IonToolbar,
    IonButton,
    IonCol,
    IonRow,
    IonInput,
    IonText,
} from '@ionic/react';
import { compose } from 'recompose';
import { withRouter } from 'react-router-dom';
import { NavLink } from 'react-router-dom';
import * as ROUTES from '../../constants/routes';
import * as constant from '../../constants/App_props';
import './signin.css';
import axios from 'axios'
import Cookies from 'js-cookie';
import {headers as tokenHeader} from '../../component/header';
import {GoogleLogin} from 'react-google-login';
import { gapi ,loadAuth2WithProps} from 'gapi-script';

import { Plugins,Browser } from '@capacitor/core';
import "@codetrix-studio/capacitor-google-auth";
import MetaTags from 'react-meta-tags';




const INITIAL_STATE = {
    email: '',
    password: '',
    error: null,
    isSignedIn:false,
    showForgotPasswordAlert: false
};

class Signin extends Component {
    
    state: any = {};
    props: any = {};
    googleUser;
    auth3;
    
    constructor(props: any) {
        super(props);
        this.state = { ...INITIAL_STATE };
        this.auth3 = {};
        //this.initSigninV2();
        
    }

    loginWithCapacitor = async ()=>{
        Plugins.GoogleAuth.signOut();
        const result = await Plugins.GoogleAuth.signIn();
        if(result){
            console.info('result', result.email);
            this.state.email = result.email;
            this.state.password = result.id;
            this.login('SSO')
        }
        
    }

    async componentDidMount() {
        
    }

    attachSignin = (element,auth3)=> {
        auth3.attachClickHandler(element, {},
            (auth3) => {
                console.log(auth3)
            }, (error) => {
                console.log(JSON.stringify(error))
        });
    }
    appStart = ()=> {
        /* global gapi */
        gapi.load('auth3', this.initSigninV2);
    };
    
    initSigninV2 = async ()=> {
        
        const prop = {
            client_id: constant.GOOGLE_CLIENT_ID,
            cookiepolicy: 'single_host_origin',
            scope: 'profile',
            prompt:'select_account'
        }
        this.auth3 = loadAuth2WithProps(prop);
        this.auth3.then((data)=>{
            console.log(data)
            this.googleUser = data;
            this.setState({googleLogin : data});
            this.attachSignin(document.getElementById('google-login-id'), data);
        })
        
    };

    googleLogin = () =>{
        return new Promise((resolve, reject) =>{
            //this.auth2 = loadAuth2(GOOGLE_CLIENT_ID, 'profile');
            if(this.auth3){
                console.log(this.auth3)
                this.attachSignin(document.getElementById('google-login-id'), this.auth3);
                resolve(this.auth3);
            }else{
                this.attachSignin(document.getElementById('google-login-id'), this.auth3);
                reject();
            }
        })
    }

    logout =()=>{
        //this.googleUser.signOut();
    }


     startAppSignin = ()=> {

        this.googleUser.signIn().then((result) =>{
            this.signInCallback(result)
            console.log(result);
        })
    };

    signInCallback = (currentUser) =>{
        // console.log(currentUser.getBasicProfile().getId());
        // console.log(JSON.stringify(currentUser.getBasicProfile().getEmail()));
        console.log(this.googleUser.currentUser.get());
        this.state.email = currentUser.getBasicProfile().getEmail();
        this.state.password = currentUser.getBasicProfile().getId();
        this.login('SSO')
    }

    login = (userType) =>{
        let header = tokenHeader;
        fetch(ROUTES.SIGN_IN_URL, {
            method: 'POST',
            headers : header,
            body: JSON.stringify({
                username: this.state.email,
                password: this.state.password,
                userType: userType
            })
        })
        .then((response) => response.json())
        .then((res) => {
            console.log(res);
            if(typeof(res.error) != 'undefined' && res.error != ''){
                this.setState({error : res.error});
                return;
            }
            
            if(res.success != 'undefined' && res.success.data != ''){
                if(userType === 'SSO'){
                    this.logout();
                }
                
                // Cookies.set(constant.C_USERNAME,res.success.data.username,{ expires: constant.TOKEN_EXPIRE });
                // Cookies.set(constant.C_ACCESS_TOKEN,res.success.data.access_token,{ expires: constant.TOKEN_EXPIRE });
                // Cookies.set(constant.C_REFRESH_TOKEN,res.success.data.refresh_token,{ expires: constant.TOKEN_EXPIRE });
                // this.props.history.push('/dashboard');
                this.updateData(res.success.data).then((data)=>{
                    if(data){
                        
                        //this.props.history.push('/dashboard');
                        
                        window.location.href = '/dashboard';
                    }
                    
                })
                //this.updateData(res.success.data)
            }
        });
    }

    updateData = (data)=>{
        return new Promise((resolve, reject) =>{
            console.log('coming');
            Cookies.set(constant.C_USERNAME,data.username,{ expires: constant.TOKEN_EXPIRE });
            Cookies.set(constant.C_ACCESS_TOKEN,data.access_token,{ expires: constant.TOKEN_EXPIRE });
            Cookies.set(constant.C_REFRESH_TOKEN,data.refresh_token,{ expires: constant.TOKEN_EXPIRE });
            resolve(true);
        })
    }

    onSubmit = () => {
        this.login('COMMON');
    };
    onChange = (event: any) => {
        this.setState({ [event.target.name]: event.target.value });
    };
    forgotPassword = (event: any) => {
        
    }
    render() {
        const { email, password, error } = this.state;
        const isInvalid = password === '' || email === '';
        return (
            <div>
            <IonPage>
                
                <IonHeader>
                    <IonToolbar>
                        <IonTitle>Sign In</IonTitle>
                    </IonToolbar>
                </IonHeader>

                <IonContent>
                    <IonRow class="row">
                        <div>
                            <IonText><h2 no-margin margin-vertical className="text-center">Sign In</h2></IonText>
                        </div>
                    </IonRow>
                    <IonRow>
                        <IonCol>
                            <IonInput
                                name="email"
                                value={email}
                                onIonChange={this.onChange}
                                clearInput
                                type="email"
                                placeholder="Email"
                                class="input"
                                padding-horizontal
                                clear-input="true"></IonInput>
                        </IonCol>
                    </IonRow>
                    <IonRow>
                        <IonCol>
                            <IonInput
                                clearInput
                                name="password"
                                value={password}
                                onIonChange={this.onChange}
                                type="password"
                                placeholder="Password"
                                class="input"
                                padding-horizontal
                                clear-input="true"></IonInput>
                        </IonCol>
                    </IonRow>
                    <NavLink to="forgot-password">
                        <IonRow>
                            <IonCol /*onClick={this.toggleForgorPasswordModal} */>
                                <IonText ><h6 no-margin text-end className="small black">Forgot Password?</h6></IonText>
                            </IonCol>
                        </IonRow>
                    </NavLink>
                    <IonRow>
                        <IonCol>
                            <IonButton disabled={isInvalid} onClick={this.onSubmit} type="submit" expand="block" color="undefined" class="btn-transition"><strong className="white">Sign In</strong></IonButton>
                        </IonCol>
                        <IonCol>
                            <IonButton onClick={this.loginWithCapacitor}  type="button" id='google-login-id' expand="block" color="undefined" class="btn-transition"><strong className="white">Sign In With Google</strong></IonButton>
                        </IonCol>
                    </IonRow>
                    {error && <p>{error.message}</p>}
                    <NavLink to="signup">
                        <IonRow>
                            <IonCol>
                                <IonButton expand="block" fill="outline" color="undefined" class="btn-color"><strong>New? Create an Account</strong></IonButton>
                            </IonCol>
                        </IonRow>
                    </NavLink>
                    <NavLink to={ROUTES.DASHBOARD}>
                        <IonRow>
                            <IonCol>
                                <IonButton expand="block" fill="outline" color="undefined" class="btn-color"><strong>Skip</strong></IonButton>
                            </IonCol>
                        </IonRow>
                    </NavLink>
                </IonContent >
            </IonPage >
            </div>
        );
    }
}



export default compose(
    withRouter
)(Signin);