import React, {useEffect} from 'react';
import ReactDOM from 'react-dom';

// Ionic 4 styles
import '@ionic/core/css/core.css';
import '@ionic/core/css/ionic.bundle.css';

import './index.css';
import App from './App';
import Signin from './pages/signin/signin'
import * as serviceWorker from './serviceWorker';
import axios from 'axios';
import {headers as tokenHeader, getToken} from './component/header';
import * as constant from './constants/App_props';
import Cookies from 'js-cookie';


axios.defaults.headers.common = {
  'Access-Control-Allow-Origin' : '*',
  'access-control-allow-headers' : 'GET,PUT,POST,DELETE,PATCH,OPTIONS',
  'Content-Type' : 'application/json',
  'Accept' : 'application/json;charset=utf-8',
  'Access-Control-Allow-Methods' : 'GET,PUT,POST,DELETE,PATCH,OPTIONS',
  // "Access-Control-Allow-Credentials":"true",
  "Authorization" : 'Bearer '+Cookies.get(constant.C_ACCESS_TOKEN)
};

axios.defaults.validateStatus = function (status) {
  console.log(status);
  console.log(status >= 200 && status < 300);
  return status >= 200 && status < 300;
}

axios.interceptors.request.use(request => {
  console.log(request);
  
  //request.headers = tokenHeader;
  
  // Edit request config
  return request;
}, error => {
  console.log(error);
  return Promise.reject(error);
});

axios.interceptors.response.use(response => {
  // Edit response config
  console.log(response)
  return response;
}, error => {
  console.log(error);
  return Promise.reject(error);
});


ReactDOM.render(
  <React.StrictMode>
    <App/>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
