import React, { Component, useEffect } from 'react';
import { BrowserRouter, BrowserRouter as Router, Redirect, Route, Switch } from 'react-router-dom';
import logo from './logo.svg';
import {IonApp, IonPage} from '@ionic/react';
import './App.css';

import Signin from './pages/signin/signin'
import Dashboard from './pages/dashboard/dashboard'
import * as ROUTES from './constants/routes';
import {PrivateRoute} from './component/PrivateRoute'
import Maps from './pages/maps/maps';
import { Plugins, Capacitor } from "@capacitor/core";


class App extends Component {
  constructor(props) {
    super(props)
    Plugins.App.addListener('backButton', () => {
      Plugins.App.exitApp();
    });
  }

  initializeApp() {
    
  }



  render(){
    return (
      <Router>
        <IonApp>
          <IonPage >
            <BrowserRouter>
          <Switch>
                {/* <Route exact path={ROUTES.SIGN_UP} component={Signup} /> */}
                <Route exact={true} path={ROUTES.SIGN_IN} component={Signin} />
                <Route exact={true} path={ROUTES.MAPS} component={Maps} />
                <PrivateRoute exact path={ROUTES.DASHBOARD} component={Dashboard} />
                
                {/* <Route exact path={ROUTES.FORGOT_PASSWORD} component={ForgotPassword} /> */}
                {/* <Route exact path="/" component={IndexView} /> */}
                <Redirect to={ROUTES.SIGN_IN}/>
              </Switch>
              </BrowserRouter>
          </IonPage>
        </IonApp>
      </Router>
      
  );
  }
}

// function App() {
//   return (
//     <div className="App">
//       <header className="App-header">
//         <img src={logo} className="App-logo" alt="logo" />
//         <p>
//           Edit <code>src/App.tsx</code> and save to reload.
//         </p>
//         <a
//           className="App-link"
//           href="https://reactjs.org"
//           target="_blank"
//           rel="noopener noreferrer"
//         >
//           Learn React
//         </a>
//       </header>
//     </div>
//   );
//}

export default App;
