export const SIGN_UP = '/signup';
export const SIGN_IN = '/signin';
export const MAPS = '/maps';
export const FORGOT_PASSWORD = '/forgot-password';
export const DASHBOARD = '/dashboard';
//export const API_URL = 'http://192.168.5.242/';
export const CONTEXT_PATH = 'action-tracker-api/';
export const API_URL = 'https://tracker.onudaan.com/'+CONTEXT_PATH;
export const SIGN_IN_URL = API_URL+'rest/auth/login';

export const DASHBOARD_PROJECTLIST = API_URL+'dashboard/projectList';